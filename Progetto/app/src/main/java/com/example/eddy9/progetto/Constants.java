package com.example.eddy9.progetto;

public class Constants {


        public static final String STDBY_SUFFIX = "-stdby";
        public static final String PUB_KEY = "pub-c-a5598f9b-bf67-40cb-b63f-c9dcff35bed7"; // Use Your Pub Key
        public static final String SUB_KEY = "sub-c-861fc0da-769c-11e6-acd7-0619f8945a4f"; // Use Your Sub Key
        public static final String USER_NAME = "user_name";
        public static final String JSON_CALL_USER = "call_user";
        public static final String SHARED_PREFS = ".SHARED_PREFS";
        public static final String LAST_CALLED = "last_user_called";
        public static final String CALL_USER ="call_user" ;
}
