package com.example.eddy9.progetto;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


//Activity che si apre solo quando non vi è nessun username memorizzato nelle sharedpreferences, o dopo un signout
    public class LoginActivity extends Activity {

        private EditText Username;
        private Button sign_in;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            Username = (EditText) findViewById(R.id.login_username);
            sign_in =(Button) findViewById(R.id.button_sign);
            sign_in.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    joinChat(v);
                }
            });
            Bundle extras = getIntent().getExtras();
            if (extras != null){
                String lastUsername = extras.getString("oldUsername", "");
                Username.setText(lastUsername);
            }
        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {

            getMenuInflater().inflate(R.menu.menu_login, menu);
            return true;
        }
        //controllo sul nome
        public void joinChat(View view){
            String username = Username.getText().toString();
            if (!validUsername(username))
                return;

            SharedPreferences sp = getSharedPreferences(Constants.SHARED_PREFS,MODE_PRIVATE);
            SharedPreferences.Editor edit = sp.edit();
            edit.putString(Constants.USER_NAME, username);
            edit.apply();

            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }


        private boolean validUsername(String username) {
            if (username.length() == 0) {
                Username.setError("Username non può essere vuoto.");
                return false;
            }
            if (username.length() > 16) {
                Username.setError("Username troppo lungo.");
                return false;
            }
            return true;
        }
}
