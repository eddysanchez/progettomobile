package com.example.eddy9.progetto;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubException;

import org.json.JSONException;
import org.json.JSONObject;
import com.pubnub.api.Callback;

import java.util.ArrayList;
import java.util.List;


/*Per quanto riguarda il processo di signaling ( quindi per poter trovare e mettere quindi in comunicazione i peer
  ho deciso di utilizzare le API di Punnub.
  Sostanzialmente, utlizzando una coppia di chiavi riesco a definire univocamente la "room" dove i peer si metteranno in comunicazione*/



public class MainActivity extends Activity {
    private SharedPreferences mSharedPreferences;
    private TextView mUsername;
    private EditText mCallNumET;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private String username;
    ListView listView;
    ArrayAdapter listAdapter;
    private Pubnub pubNub;
    String[] permissions= new String[]{
            Manifest.permission.RECORD_AUDIO,
            android.Manifest.permission.CAMERA,
            };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(checkPermissions()) {
            this.mSharedPreferences = getSharedPreferences(Constants.SHARED_PREFS, MODE_PRIVATE);
            //Controllo delle sharepreferences; in caso non fossero presenti, si tornerà alla schermata di login
            if (!this.mSharedPreferences.contains(Constants.USER_NAME)) {
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
            this.username = this.mSharedPreferences.getString(Constants.USER_NAME, "");


             listView = (ListView)findViewById(R.id.lastCalled);
             ArrayAdapter<String> listAdapter = new ArrayAdapter(this, R.layout.row);
            listView.setAdapter(listAdapter);
            this.mCallNumET = (EditText) findViewById(R.id.call_num);
            this.mUsername = (TextView) findViewById(R.id.main_username);

            this.mUsername.setText("Sei loggato come :" + this.username);
            //inizializzazione della connessione
            initPubNub();

        }else{
            Toast.makeText(MainActivity.this, "Non hai i permessi necessari", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
//qui ho scelto di appoggiarmi a PunNub per quanto riguarda la signaling dei peers
    public void initPubNub() {
        String stdbyChannel = this.username + Constants.STDBY_SUFFIX;
        this.pubNub = new Pubnub(Constants.PUB_KEY, Constants.SUB_KEY);
        this.pubNub.setUUID(this.username);

        try {
            this.pubNub.subscribe(stdbyChannel, new Callback() {
                @Override
                public void successCallback(String channel, Object message) {
                    Log.d("MA-success", "MESSAGE: " + message.toString());
                    if (!(message instanceof JSONObject)) return; // check sul messaggioJSON
                    JSONObject jsonMsg = (JSONObject) message;
                    try {
                        if (!jsonMsg.has(Constants.JSON_CALL_USER)) return;
                        String user = jsonMsg.getString(Constants.JSON_CALL_USER);
                        dispatchIncomingCall(user);//a questo punto di dovrebbe aprire l'activity relativa alla chiamata in arrivo
                        Intent intent = new Intent(MainActivity.this, VideoChatActivity.class);
                        intent.putExtra(Constants.USER_NAME, username);
                        intent.putExtra(Constants.JSON_CALL_USER, user);
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (PubnubException e) {
            e.printStackTrace();
        }
    }

    //funzione che controlla i vari permessi
    private  boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p:permissions) {
            result = ContextCompat.checkSelfPermission(MainActivity.this,p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }
    //aggiunge le opzioni all'action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case R.id.action_sign_out:
                signOut();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    //oltre a effettuare il logout, elimina anche l'username precendentemente registrato dalle shared preferences
    public void signOut(){
        SharedPreferences.Editor edit = this.mSharedPreferences.edit();
        edit.remove(Constants.USER_NAME);
        edit.apply();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("oldUsername", this.username);
        startActivity(intent);
    }


    //funzione che dovrebbe gestire la chiamata in arrivo, richiamando l'activity incomingActivity
    private void dispatchIncomingCall(String userId){
//        makeToast(userId + "ti sta chiamando");
        Intent intent = new Intent(MainActivity.this, IncomingActivity.class);
        intent.putExtra(Constants.USER_NAME, username);
        intent.putExtra(Constants.CALL_USER, userId);
        startActivity(intent);
    }
    public void makeCall(View view){

        String callNum = mCallNumET.getText().toString();//controllo sull'username da chiamare
        if (callNum.isEmpty() || callNum.equals(this.username)) {
            Toast.makeText(this, "Enter a valid user.", Toast.LENGTH_SHORT).show();
            listAdapter.add(callNum);

        }
        dispatchCall(callNum);
    }


    public void dispatchCall(final String callNum) {
        final String callNumStdBy = callNum + Constants.STDBY_SUFFIX;
        JSONObject jsonCall = new JSONObject();
        try {
            jsonCall.put(Constants.JSON_CALL_USER, this.username);
            pubNub.publish(callNumStdBy, jsonCall, new Callback() {
                @Override
                public void successCallback(String channel, Object message) {
                    Log.d("MA-dCall", "SUCCESS: " + message.toString());
                    Intent intent = new Intent(MainActivity.this, VideoChatActivity.class);
                    intent.putExtra(Constants.USER_NAME, username);
                    intent.putExtra(Constants.CALL_USER, callNum);
                    startActivity(intent);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }




}